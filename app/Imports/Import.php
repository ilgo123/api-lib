<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use RealRashid\SweetAlert\Facades\Alert;

class Import implements ToCollection, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row){
            DB::table('getroutev2')->insert([
                //'peer'                  => $row['intconname'],
                //'caller_number'         => $row['cidnumber'], 
                'destination_number'    => $row['dstnumber'],
                'primary_route'         => $row['primary'],
                'secondary_route'       => $row['secondary']
            ]);
        }
        
        Alert::success('Successfully Imported Data');
        return;
    }
}
