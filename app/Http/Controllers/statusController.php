<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class statusController extends Controller
{
    public function update(Request $request)
    {
        $numberMsisdn = $request->msisdn;
        $status = intval($request->status);

        $validator = Validator::make($request->all(), [
            "msisdn" => "required",
            "status" => "required"
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors()->toArray()], 400);
        }

        try {
            $updateStatusQuery = DB::table('getroutev3')->where("destination_number", "+" . $numberMsisdn)->update([
                "status" => $status
            ]);

            return response()->json(["status" => 200, "message" => "status has been updated"], 200);
        } catch (\Throwable $th) {
            return response()->json(["message" => $th->getMessage(), "status" => 400], 400);
        }
    }
}
