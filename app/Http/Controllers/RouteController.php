<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Imports\Import;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class RouteController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function import()
    {
        Excel::import(new Import, request()->file('file'));

        return back();
    }

    public function getRoute(Request $request)
    {
	ini_set('memory_limit', '-1');        
$peer = $request->intconname;
        $cnumber = $request->cidnumber;
        $dnumber = $request->dstnumber;

        $validator = Validator::make($request->all(), [
            "intconname" => "required",
            "cidnumber" => "required",
            "dstnumber" => "required"
        ]);

        if ($validator->fails()) {
            return response()->json([$validator->errors()->toArray()], 400);
        }
	
	try {
            if(strncmp($dnumber, 67073, 5) === 0 || strncmp($dnumber, 67074, 5) === 0) {
                $v1 = DB::table('getroutev2')
			->select('primary_route', 'secondary_route')
                        ->where('destination_number', "+" . $dnumber)
                        ->get();

                if (count($v1) === 0) {
                    return response()->json(["MSC", "MSC"], 200);
                } else {
                    return response()->json([$v1[0]->primary_route, $v1[0]->secondary_route], 200);
                }
            }

            if($peer == "MSC"){
                $data = DB::table('getroutev2')->get(['destination_number', 'primary_route']);

                foreach ($data as $dt) {
                    if (strncmp($dnumber, $dt->destination_number, strlen($dt->destination_number)) === 0) {
                        $tx_pri = $dt->primary_route == '110' ? 'TELIN_GP_SG' : ($dt->primary_route == '112' ? 'TELIN_GP_HK' : ($dt->primary_route == '141' ? "TELIN_IP_HK" : $dt->primary_route));
                        return response()->json([$tx_pri, $tx_pri], 200);
                    } else {
                        continue;
                    }
                }
            }
            return response()->json(["Not Found"], 400);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}
