<?php

use App\Http\Controllers\RouteController;
use App\Http\Controllers\fixingController;
use App\Http\Controllers\routeControllerv3;
use App\Http\Controllers\fixingControllerV2;
use App\Http\Controllers\statusController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('route', [fixingController::class, 'getRoute']);
Route::get('test', [fixingControllerV2::class, 'test']);
Route::get('update-status', [statusController::class, 'update']);
