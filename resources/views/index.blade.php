<!DOCTYPE html>

<html>

<head>
    <title>LIB IMPORT</title>
    <link rel="stylesheet" href="{{ asset('bootstrap/css/bootstrap.min.css') }}">
</head>

<body>
    <div class="container mt-3">
        {{-- <div class="card bg-light mt-3"> --}}
            {{-- <div class="card-body"> --}}
                <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data" >
                    @csrf
                    <div class="row w-100">
                        <div class="col-12 d-flex justify-content-between"><input type="file" name="file" class="form-control"><button class="btn btn-success ms-4">Import</button></div>
                        
                    </div>
                </form>
            {{-- </div> --}}
        {{-- </div> --}}
    </div>
    
    <script src="{{ asset('sweetalert/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    @include('sweetalert::alert')
</body>
</html>